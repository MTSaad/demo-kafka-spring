package com.biollca.demospringcloudstreamskafka.service;


import com.biollca.demospringcloudstreamskafka.entites.PageEvent;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.internals.TimeWindow;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Service
public class PageEventService {
    /**
     * Deuxième cas
     * Pour créer un consumer avec une méthode qui va retouner une fonction nommée consumer, elle va consommer notre entitie PageEvent
     * Pour la deployer on utilise la notation @Bean
     * **/
    @Bean
    public Consumer<PageEvent> pageEventConsumer(){
        return (input) -> {
            System.out.println("*****************************");
            System.out.println(input.toString());
            System.out.println("*****************************");
        };
    }


   /**
    * Troisième cas
    * Création d'un Producer Puller en utilisant une fonction de type Supplier (un fornisseur de données) pour envoyer un message chaque seconde*
    * On peut l'utiliser dans un autre contexte par exemple : pour récupérer à chaque seconde des infos à partir de notre base de données et faire des traitements dessus
    * produire un événement vers un Topic */

    @Bean
     public Supplier<PageEvent> pageEventSupplier(){
         return ()-> new PageEvent(Math.random()>0.5?"Page1":"Page2",
                 Math.random()>0.5?"User1":"User2",
                 new Date(),
                 new Random().nextInt(9000));

        }

   /**
    * 4ème cas
    * Consumer & Producer on reçoit en entrée un flux d'enregistrement en IN,
    * on le traite et on produit un autre stream qui va en sortie OUT qui va vers un autre Topic nommé R3 par exemple
    * On utilise une Function cette fois-ci
    * **/
   @Bean
   public Function<PageEvent,PageEvent> pageEventFunction(){
       return (input) -> {
                input.setName("Page Test");
                input.setUser("USER");
                return input;
       };

   }

    /**
     * 5 ème cas
     * On utilise une Function KStream avec une clé et une valeur
     * Le Topic R4 va contenir la clé = nom de la page
     * la valeur = le nombre de fois dans lequel la page a été visitée
     *  Pour tester sur la kafka-consumer-console on lance la commande suivante :
     *  >start bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic R4 --property print.key=true --property print.value=true --property key.deserializer=org.apache.kafka.common.serialization.StringDeserialization --property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer
     *
     * **/
    @Bean
    public Function<KStream<String,PageEvent> , KStream<String,Long>> kStreamFunction(){
          return (input) ->{
                 return input
                         .filter((k,v)-> v.getDuration()>100)
                        .map((k,v) -> new KeyValue<>(v.getName(),0L))
                         .groupBy((K,V)-> K,Grouped.with(Serdes.String(),Serdes.Long()))
                         .count()
                         .toStream();

          };

    }
}
