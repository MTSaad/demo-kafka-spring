package com.biollca.demospringcloudstreamskafka.web;

import com.biollca.demospringcloudstreamskafka.entites.PageEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Random;

/**Premier cas
 * on le test sur le terminal avec kafka-console-consumer en lançant dans le navigateur http://localhost:8080/publish/R1/blog
 * R1 nom du topic
 * blog nom de la page
 *  **/
@RestController
public class PageEventRestController {
    /** injection d'un objet de type StreamBridge **/
    @Autowired
    private StreamBridge streamBridge;

    /** publier un evenement dans un Topic kafka avec une méthode qui prend deux params le nom du topic et le nom de la page **/
    @GetMapping("/publish/{topic}/{name}")
    public PageEvent publish(@PathVariable String topic, @PathVariable String name){
        PageEvent pageEvent = new PageEvent(name,Math.random()>0.5?"User1":"User2", new Date(),new Random().nextInt(9000));
        streamBridge.send(topic,pageEvent);
        return pageEvent;

    }
}
